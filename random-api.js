const axios = require("axios");

class JsonApi {
  async getApi() {
    const response = await axios.get(
      "https://jsonplaceholder.typicode/posts/1"
    );
    return response;
  }

  async postApi() {
    const response = await axios.post(
      "https://jsonplaceholder.typicode/posts",
      {
        userId: 99,
        id: 1,
        title: "hello",
        body: "world",
      }
    );

    return response;
  }
}

module.exports = new JsonApi();
