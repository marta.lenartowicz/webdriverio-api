const axios = require("axios");
const jsonapi = require("../tests/random-api");

describe("api tests", () => {
  it("get request", () => {
    const getapi = browser.call(async () => await jsonapi.getApi());

    console.log(getapi);
  });

  it("post api", () => {
    const postapi = browser.call(async () => await jsonapi.postApi());

    console.log(postapi);
  });
});

// const axios = require("axios");

// class JsonApi {
//   async getApi() {
//     const response = await axios.get(
//       "https://jsonplaceholder.typicode/posts/1"
//     );
//     return response;
//   }

//   async postApi() {
//     const response = await axios.post(
//       "https://jsonplaceholder.typicode/posts",
//       {
//         userId: 99,
//         id: 1,
//         title: "hello",
//         body: "world",
//       }
//     );

//     return response;
//   }
// }

// module.exports = new JsonApi();
